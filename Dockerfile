FROM python:3.7
ENV PYTHONUNBUFFERED 1
WORKDIR /src
COPY ./ ./
RUN pip3 install -r requirements.txt && apt-get clean