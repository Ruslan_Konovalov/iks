#!/usr/bin/env bash
uvicorn service:app --workers 2 --reload --port 8000 --host 0.0.0.0 --log-level error