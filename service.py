import httpx
import uvicorn
import requests
from fastapi import FastAPI
from fastapi.responses import JSONResponse


app = FastAPI()


@app.get("/")
def root():
    return {"message": "Hello World"}
